// Array globale per ruajtjen e kategorive
categories = [];

// Kategoria e selektuar
selectedCategoryId = -1;

// Array associative per ruajten e artikujve
products = {};

// Array globale per ruajtjen e klientave te castit
clients = [];

// Array globale per ruajten e fjaleve per kerkimin e artikujve
searchProducts = [];

// Array globale per ruajtjen e kartave dhurate
giftCards = [];

// Objekti fature
invoice = {
    productQuantity: 0,
    productTotal: 0.0,
    discountRate: 0.0,
    isDiscountRate: false,
    isDiscountForSubtotals: false,
    discount: 0.0,
    koperturaRate: 0.0,
    isKoperturaRate: false,
    kopertura: 0.0,
    invoiceTotal: 0.0,
    client: {},
    invoiceProducts: [],
    recalculateInvoice: function() {

        var quantitySum = 0;
        var productTotal = 0;

        $.each(this.invoiceProducts, function(index, product) {
            quantitySum += Number(product.quantity);


            if (invoice.isDiscountForSubtotals) {
                product.subtotal = (Number(product.quantity) * Number(product.productObject.productPrice)) - Number(invoice.discount);
            }
            else {
                product.subtotal = Number(product.quantity) * Number(product.productObject.productPrice);
            }

            productTotal += product.subtotal;
            product.div.find(".subtotal").first().text(round(product.subtotal));
        });

        this.productQuantity = quantitySum;
        $("#invoice_product_quantity").text(round(this.productQuantity));
        $("#payment_product_quantity").text(round(this.productQuantity));

        this.productTotal = productTotal;
        $("#invoice_product_total").text(round(this.productTotal));

        this.recalculateInvoiceTotal();
    },
    setDiscountRate: function(discountRate) {
        this.isDiscountRate = true;
        this.discountRate = discountRate;
        this.discount = (this.discountRate * this.productTotal) / 100;
        $("#invoice_discount").text(round(this.discount));

        this.recalculateInvoice();
    },
    setDiscount: function(discount) {
        if (discount.indexOf("%") == discount.length - 1 && !isNaN(discount.substring(0, discount.length - 1))) {
            this.setDiscountRate(discount.substring(0, discount.length - 1));
        }
        else if (!isNaN(discount)) {
            this.isDiscountRate = false;
            this.discount = discount;
            $("#invoice_discount").text(round(this.discount));

            this.recalculateInvoice();
        }
    },
    setKoperturaRate: function(koperturaRate) {
        this.isKoperturaRate = true;
        this.koperturaRate = koperturaRate;
        this.kopertura = (this.koperturaRate * this.productTotal) / 100;
        $("#invoice_kopertura").text(round(this.kopertura));

        this.recalculateInvoice();
    },
    setKopertura: function(kopertura) {
        if (kopertura.indexOf("%") == kopertura.length - 1 && !isNaN(kopertura.substring(0, kopertura.length - 1))) {
            this.setKoperturaRate(kopertura.substring(0, kopertura.length - 1));
        }
        else if (!isNaN(kopertura)) {
            this.isKoperturaRate = false;
            this.kopertura = kopertura;
            $("#invoice_kopertura").text(round(this.kopertura));
            this.recalculateInvoice();
        }
    },
    recalculateInvoiceTotal: function() {

        if (this.isKoperturaRate) {
            this.kopertura = (this.koperturaRate * this.productTotal) / 100;
            $("#invoice_kopertura").text(round(this.kopertura));
        }

        if (this.isDiscountRate) {
            this.discount = (this.discountRate * this.productTotal) / 100;
            $("#invoice_discount").text(round(this.discount));
        }

        if (this.isDiscountForSubtotals) {
            this.invoiceTotal = this.productTotal + Number(this.kopertura);
        }
        else {
            this.invoiceTotal = this.productTotal - Number(this.discount) + Number(this.kopertura);
        }

        $("#invoice_total").text(round(this.invoiceTotal));
        $("#payment_total").text(round(this.invoiceTotal));
        $("#invoice_value_button").text(round(this.invoiceTotal));
    },
    addProductDiv: function(product) {

        $("#tbody-products").append(`
            <tr id="product_${product.productId}">
                <td>${product.productDescription}</td>
                <td class="price">${product.productPrice}</td>
                <td>
                    <input type="number" class="quantity" value="1" min="1" style="width: 40px; height: 18px; text-align: right" />
                </td>
                <td class="subtotal">${product.productPrice}</td>
                <td>
                    <a role="button" style="color: black;">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        `);

        this.addProductBindings(product);
    },
    addProductBindings: function(product) {

        var deleteProductButton = $(`#product_${product.productId}`)
            .children().children("a").first();

        deleteProductButton.click(function(ev) {

            var deletedProduct = $(this).parent().parent();
            var deletedProductId = deletedProduct.attr("id");

            invoice.removeProduct(deletedProductId);
        });

        var quantityElement = $(`#product_${product.productId}`)
            .children().children(".quantity").first();

        quantityElement.on("keyup input", function() {
            var id = $(this).parent().parent().attr("id");
            var subtotalElement = $(this).parent().siblings(".subtotal").first();
            var price = $(this).parent().siblings(".price").first().text();
            var quantity = $(this).val();
            subtotalElement.text(round(price * quantity));
            $.grep(invoice.invoiceProducts,
                function(productDiv) { return productDiv.divId == id; })[0].setQuantity(quantity);
        });
    },
    addProduct: function(product) {

        var isProductDuplicate = false;
        var invoiceProd;

        for (var i = 0; i < this.invoiceProducts.length; i++) {
            if(this.invoiceProducts[i].productObject.productId == product.productId) {
                isProductDuplicate = true;
                invoiceProd = this.invoiceProducts[i];
            }
        }

        if (!isProductDuplicate) {

            this.addProductDiv(product);

            this.invoiceProducts.push({
                divId: `product_${product.productId}`,
                div: $(`#product_${product.productId}`),
                productObject: product,
                quantity: 1,
                subtotal: product.productPrice,
                incrementQuantity: function() {
                    var quantityElement = this.div.children().children(".quantity").first();
                    quantityElement.val(Number(quantityElement.val()) + 1);
                    var subtotalElement = this.div.children(".subtotal").first();
                    var price = this.div.children(".price").first().text();
                    subtotalElement.text(Math.round(price * quantityElement.val() * 100) / 100);
                    this.quantity++;
                    invoice.recalculateInvoice();
                },
                setQuantity: function(quantity) {
                    this.quantity = quantity;
                    invoice.recalculateInvoice();
                }
            });

            this.recalculateInvoice();
        }
        else {
            invoiceProd.incrementQuantity();
        }

    },
    removeProduct: function(deletedProductId) {

        $(`#${deletedProductId}`).remove();

        var deletedProductPrice;

        for (var i = 0; i < this.invoiceProducts.length; i++ ) {
            if (this.invoiceProducts[i].divId == deletedProductId ) {
                deletedProductPrice = this.invoiceProducts[i].productObject.productPrice;
                this.invoiceProducts.splice(i, 1);
            }
        }

        this.recalculateInvoice();
    },
    addGiftCard: function(giftCard) {

        $("#tbody-products").append(`
            <tr id="giftcard_${giftCard.cardNumber}">
                <td>Karte dhurate</td>
                <td class="price">${giftCard.price}</td>
                <td>
                    <input type="text" value="1" style="width: 40px; height: 18px; text-align: center" readOnly />
                </td>
                <td class="subtotal">${giftCard.price}</td>
                <td>
                    <a role="button" style="color: black;">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        `);

        var deleteGiftCardButton = $(`#giftcard_${giftCard.cardNumber}`)
            .children().children("a").first();

        deleteGiftCardButton.click(function(ev) {

            var deletedGiftCard = $(this).parent().parent();
            var deletedGiftCardNumber = deletedGiftCard.attr("id");
            deletedGiftCard.remove();

            for (var i = 0; i < invoice.invoiceProducts.length; i++ ) {

                if (invoice.invoiceProducts[i].divId == deletedGiftCardNumber ) {

                    invoice.invoiceProducts.splice(i, 1);
                }
            }

            invoice.recalculateInvoice();
        });

        this.invoiceProducts.push({
            divId: `giftcard_${giftCard.cardNumber}`,
            div: $(`#giftcard_${giftCard.cardNumber}`),
            productObject: {
                productId: giftCard.cardNumber,
                productDescription: "Karte Dhurate",
                productPrice: giftCard.price
            },
            quantity: 1,
            value: giftCard.value,
            expirationDate: giftCard.expirationDate
        });

        this.recalculateInvoice();
    },
    isGiftCardNumberUnique: function(cardNumber) {

        for (var i = 0; i < this.invoiceProducts.length; i++) {
            if (this.invoiceProducts[i].productObject.productId == cardNumber) {
                return false;
            }
        }

        return true;
    },
    setClient: function(client) {
        this.client = client;
    },
    reset: function() {
        this.productQuantity = 0;
        this.productTotal = 0;
        this.discountRate = 0;
        this.isDiscountRate = false;
        this.isDiscountForSubtotals = false;
        this.discount = 0;
        this.koperturaRate = 0;
        this.isKoperturaRate = false;
        this.kopertura = 0;
        this.invoiceTotal = 0;
        this.client = {};

        for (var i = 0; i < this.invoiceProducts.length; i++) {
            this.invoiceProducts[i].div.remove();
        }

        this.invoiceProducts = [];

        this.recalculateInvoice();

        $("#invoice_kopertura").text("0");
        $("#invoice_discount").text("0");
    }
};

// Handler kur i gjithe dokumenti DOM behet load
$(function() {

    setInterval('updateClock()', 1000 );

    // Merr username-in me therritje ajax
    $.ajax({
        url: "/getUsername",
        contentType: "application/json",
        success: function(data) {
            $("#username-placeholder").text(data.username);
        }
    });

    // Merr nje liste te objekteve category si JSON
    $.ajax({
        url: "/getCategories",
        contentType: "application/json",
        success: function(data) {

            // Ciklon array-n me te gjitha kategorite te marra me AJAX
            $.each(data, function(index, category) {

                // Shton div-et me informacion per kategorite
                $("#configured-categories").append(`
                    <div id="category_${category.categoryId}" class="category-item row">
                        <div class="col-xs-3 col-md-3 category-image">
                            <img src="${category.categoryImageUrl}" style="width: 100%" />
                        </div>
                        <div class="col-xs-9 col-md-9 category-name">
                            ${category.categoryId}
                            <br />
                            ${category.categoryDescription}
                        </div>
                    </div>
                `);

                // Shton objektet category tek array globale categories
                categories.push({
                    divId: `category_${category.categoryId}`,
                    div: $(`#category_${category.categoryId}`),
                    categoryObject: category
                });
            });

            // Handler per eventin click te div-ave me klase category-item
            // Keto jane jane div-a qe permbajne informacion per kategorite
            $(".category-item").click(function(ev) {

                // Merr id-ne e kategorise se klikuar
                $(".category-selected").removeClass("category-selected");
                $(this).addClass("category-selected");
                var thisId = $(this).attr("id");
                var clickedCat = $.grep(categories, function(category) { return category.divId == thisId; })[0];
                selectedCategoryId = clickedCat.categoryObject.categoryId;

                if (products[selectedCategoryId] == null) {
                    fetchProducts();
                }
                else {
                    refreshProductsDiv();
                }

            });
        }
    });

    // Handler per eventin click per butonin me id category-button
    // Ky handler eshte per shfaqjen e divit me listen e kategorive
    $("#category-button").click(function(ev) {
        $("#main-well").toggleClass('col-md-12');
        $("#main-well").toggleClass('col-md-10');
        $("#configured-categories").toggle();
    });

    // Handler per keyup per inputin me id filter-kategorie
    // Ky handler filtron listen e kategorive
    $("#filter-kategorie").on("keyup", function() {
        categoryFilterHandler(this);
    });

    // $("#product-filter").on("keyup", function() {
    //     productFilterHandler(this);
    // });

    var shitKarteDialog = $( "#shit-karte-dhurate-dialog" ).dialog({
        autoOpen: false,
        width: 500,
        modal: true,
        "buttons": {
            "Shit Karte Dhurate": {
                text: "Shit Karte Dhurate",
                class: "btn-primary-dialog",
                click: function () {

                    shitKarteDialog.find(".input-error").text("");

                    var isValid = true;

                    var cardNumber = $("#nr_karte").attr("data-value");
                    var price = $("#cmimi").val();
                    var value = $("#vlera").val();
                    var expirationDate = $("#data_skadences").val();

                    if (!cardNumber) {
                        isValid = false;
                        $("#nr_karte_error").text("Futni numrin e kartes");
                    }
                    else if (!invoice.isGiftCardNumberUnique(cardNumber)){
                        isValid = false;
                        $("#nr_karte_error").text("Numri i kartes duhet te jete unik");
                    }

                    if (!price) {
                        isValid = false;
                        $("#cmimi_error").text("Futni cmimin");
                    }
                    else if (isNaN(price)) {
                        isValid = false;
                        $("#cmimi_error").text("Cmimi duhet te jete numer");
                    }

                    if (!value) {
                        isValid = false;
                        $("#vlera_error").text("Futni vleren");
                    }
                    else if (isNaN(value)) {
                        isValid = false;
                        $("#vlera_error").text("Vlera duhet te jete numer");
                    }

                    if (!isValid) {
                        return;
                    }


                    var giftCard = {
                        cardNumber: cardNumber,
                        price: price,
                        value: value,
                        expirationDate: expirationDate
                    };

                    invoice.addGiftCard(giftCard);
                }
            },
            "Mbyll": {
                text: "Mbyll",
                class: "btn-secondary-dialog",
                click: function() {
                    shitKarteDialog.dialog( "close" );
                }
            }
        },
        priority: "primary",
        close: function() {

            shitKarteDialog.find("form")[ 0 ].reset();
            shitKarteDialog.find(".input-error").text("");
        }
    });

    var shitKarteForm = shitKarteDialog.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
    });

    $( "#shit-karte-dhurate-button" ).on( "click", function() {
        shitKarteDialog.dialog( "open" );
    });

    var shtoKlientDialog = $( "#shto-klient-dialog" ).dialog({
        autoOpen: false,
        width: 500,
        modal: true,
        "buttons": {
            "Shto Klient": {
                text: "Shto Klient",
                class: "btn-primary-dialog",
                click: function () {

                    shtoKlientDialog.find(".input-error").text("");

                    var clientName = $("#client_name_input").val();
                    var clientEmail = $("#client_email_input").val();
                    var clientTelephone = $("#client_telephone_input").val();

                    var isValid = true;

                    if (!clientName) {
                        isValid = false;
                        $("#client_name_error").text("Ju lutem plotesoni emrin e klientit");
                    }

                    if (!clientEmail) {
                        isValid = false;
                        $("#client_email_error").text("Ju lutem plotesoni email-in e klientit");
                    }
                    else if (!validateEmail(clientEmail)) {
                        isValid = false;
                        $("#client_email_error").text("Email-i i klientit nuk eshte i vlefshem");
                    }

                    if (!clientTelephone) {
                        isValid = false;
                        $("#client_telephone_error").text("Ju lutem plotesoni nr. e telephonit te klientit");
                    }

                    if (!isValid) {
                        return;
                    }

                    clients.push({
                        name: clientName,
                        email: clientEmail,
                        telephone: clientTelephone
                    });

                    $("#client_dropdown").append(`<li><a index="${clients.length-1}">${clientName}</a></li>`);

                    $(`#client_dropdown a[index=${clients.length-1}]`).click(function(ev) {
                        invoice.setClient(clients[$(this).attr("index")]);
                        $("#client_selection").text($(this).text());
                    });

                    shtoKlientDialog.dialog( "close" );
                }
            },
            "Mbyll": {
                text: "Mbyll",
                class: "btn-secondary-dialog",
                click: function() {
                    shtoKlientDialog.dialog( "close" );
                }
            }
        },
        priority: "primary",
        close: function() {

            shtoKlientDialog.find( "form" )[0].reset();
            // allFields.removeClass( "ui-state-error" );
        }
    });

    var shtoKlientForm = shtoKlientDialog.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
    });

    $( "#shto-klient" ).on( "click", function() {
        shtoKlientDialog.dialog( "open" );
    });

    var zbritjeDialog = $( "#zbritje-dialog" ).dialog({
        autoOpen: false,
        width: 300,
        modal: true,
        "buttons": {
            "Apliko": {
                text: "Apliko",
                class: "btn-primary-dialog",
                click: function () {
                    var discount = $("#zbritje-input").val();

                    var isValid = true;

                    if (!discount) {
                        isValid = false;
                        $("#zbritje_error").text("Ju lutem fusni zbritjen");
                    }
                    else if (isNaN(discount) &&
                        (discount.length > 1 &&
                        (isNaN(discount.substring(0, discount.length - 1)) ||
                         discount.indexOf("%") != discount.length - 1))) {

                        isValid = false;
                        $("#zbritje_error").text("Zbritja duhet te jete nje numer ose perqindje");
                    }

                    if (!isValid) {
                        return;
                    }

                    if ($('input[name=isDiscountForSubtotals]:checked').val() == "true") {
                        invoice.isDiscountForSubtotals = true;
                    }
                    else {
                        invoice.isDiscountForSubtotals = false;
                    }

                    invoice.setDiscount(discount);
                }
            },
            "Mbyll": {
                text: "Mbyll",
                class: "btn-secondary-dialog",
                click: function() {
                    zbritjeDialog.dialog( "close" );
                }
            }
        },
        priority: "primary",
        close: function() {

            zbritjeDialog.find( "form" )[0].reset();
            // allFields.removeClass( "ui-state-error" );
        }
    });

    var zbritjeForm = zbritjeDialog.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
    });

    $( "#invoice_discount_button" ).on( "click", function() {
        zbritjeDialog.dialog( "open" );
    });

    var koperturaDialog = $( "#kopertura-dialog" ).dialog({
        autoOpen: false,
        width: 300,
        modal: true,
        "buttons": {
            "Apliko": {
                text: "Apliko",
                class: "btn-primary-dialog",
                click: function () {
                    var kopertura = $("#kopertura-input").val();

                    var isValid = true;

                    if (!kopertura) {
                        isValid = false;
                        $("#kopertura_error").text("Ju lutem fusni koperturen");
                    }
                    else if (isNaN(kopertura) &&
                        (kopertura.length > 1 &&
                        (isNaN(kopertura.substring(0, kopertura.length - 1)) ||
                         kopertura.indexOf("%") != kopertura.length - 1))) {

                        isValid = false;
                        $("#kopertura_error").text("Kopertura duhet te jete nje numer");
                    }

                    if (!isValid) {
                        return;
                    }

                    invoice.setKopertura(kopertura);
                }
            },
            "Mbyll": {
                text: "Mbyll",
                class: "btn-secondary-dialog",
                click: function() {
                    koperturaDialog.dialog( "close" );
                }
            }
        },
        priority: "primary",
        close: function() {

            koperturaDialog.find( "form" )[0].reset();
            // allFields.removeClass( "ui-state-error" );
        }
    });

    var koperturaForm = koperturaDialog.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
    });

    $( "#invoice_kopertura_button" ).on( "click", function() {
        koperturaDialog.dialog( "open" );
    });

    var pagesaDialog = $( "#pagesa-dialog" ).dialog({
        autoOpen: false,
        width: 500,
        modal: true,
        "buttons": {
            "Ruaj": {
                text: "Ruaj",
                class: "btn-primary-dialog",
                click: function () {
                    pagesaDialog.find(".payment_value_error").text("");

                    var paymentValue = Number($("#payment_value").val());

                    if (!paymentValue) {
                        $("#payment_value_error").text("Pagesa eshte bosh");
                        return;
                    }
                    else if (invoice.invoiceTotal == 0) {
                        $("#payment_value_error").text("Nuk ka asnje produkt ne fature");
                        return;
                    }
                    else if (paymentValue < invoice.invoiceTotal) {
                        $("#payment_value_error").text("Nuk eshte mjaftueshem per te paguar faturen");
                        return;
                    }

                    saveInvoice(true);
                    pagesaDialog.dialog( "close" );
                }
            },
            "Mbyll": {
                text: "Mbyll",
                class: "btn-secondary-dialog",
                click: function() {

                    pagesaDialog.dialog( "close" );
                }
            }
        },
        priority: "primary",
        close: function() {

            pagesaDialog.find( "form" )[0].reset();

            $("#total_paid_value").text("0.00");
            $("#change_value").text("0.00");
            pagesaDialog.find(".input-error").text("");
        }
    });

    var pagesaForm = pagesaDialog.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
    });

    $( "#pagesa-button" ).on( "click", function() {
        pagesaDialog.dialog( "open" );
    });



    $("#random-generator").click(function(ev) {
        var min = 1000000000000000;
        var max = 10000000000000000;
        var randomNumber = Math.floor(Math.random() * (max - min)) + min;
        var randomNumberString = randomNumber.toString();
        for (var i = 0; i < randomNumberString.length; i+=5) {
            randomNumberString = randomNumberString.slice(0, i + 4) + " " + randomNumberString.slice(i + 4);
        }
        $("#nr_karte").val(randomNumberString);
        $("#nr_karte").attr("data-value", randomNumber);
    });

    $("#invoice_value_button").click(function(ev) {
        var paymentValueInput = $("#payment_value");
        var paymentValue = Number(paymentValueInput.val()) + Number($(this).text());
        paymentValueInput.val(round(paymentValue));
        $("#total_paid_value").text(round(paymentValue));
        $("#change_value").text(round(paymentValue - invoice.invoiceTotal));
    });

    $("#100_bill_button").click(function(ev) {
        var paymentValueInput = $("#payment_value");
        var paymentValue = Number(paymentValueInput.val()) + 100;
        paymentValueInput.val(round(paymentValue));
        $("#total_paid_value").text(round(paymentValue));
        $("#change_value").text(round(paymentValue - invoice.invoiceTotal));
    });

    $("#200_bill_button").click(function(ev) {
        var paymentValueInput = $("#payment_value");
        var paymentValue = Number(paymentValueInput.val()) + 200;
        paymentValueInput.val(round(paymentValue));
        $("#total_paid_value").text(round(paymentValue));
        $("#change_value").text(round(paymentValue - invoice.invoiceTotal));
    });

    $("#500_bill_button").click(function(ev) {
        var paymentValueInput = $("#payment_value");
        var paymentValue = Number(paymentValueInput.val()) + 500;
        paymentValueInput.val(round(paymentValue));
        $("#total_paid_value").text(round(paymentValue));
        $("#change_value").text(round(paymentValue - invoice.invoiceTotal));
    });

    $("#1000_bill_button").click(function(ev) {
        var paymentValueInput = $("#payment_value");
        var paymentValue = Number(paymentValueInput.val()) + 1000;
        paymentValueInput.val(round(paymentValue));
        $("#total_paid_value").text(round(paymentValue));
        $("#change_value").text(round(paymentValue - invoice.invoiceTotal));
    });

    $("#2000_bill_button").click(function(ev) {
        var paymentValueInput = $("#payment_value");
        var paymentValue = Number(paymentValueInput.val()) + 2000;
        paymentValueInput.val(round(paymentValue));
        $("#total_paid_value").text(round(paymentValue));
        $("#change_value").text(round(paymentValue - invoice.invoiceTotal));
    });

    $("#5000_bill_button").click(function(ev) {
        var paymentValueInput = $("#payment_value");
        var paymentValue = Number(paymentValueInput.val()) + 5000;
        paymentValueInput.val(round(paymentValue));
        $("#total_paid_value").text(round(paymentValue));
        $("#change_value").text(round(paymentValue - invoice.invoiceTotal));
    });

    $("#delete_invoice_value").click(function(ev) {
        $("#total_paid_value").text("0");
        $("#change_value").text("0");
        $("#payment_value").val("");
    });

    $("#payment_value").on("keyup", function(ev) {
        $("#total_paid_value").text(round(this.value));
        $("#change_value").text(round(this.value - invoice.invoiceTotal));
    });

    $("#ruaj-fature-button").click(function(ev) {
        saveInvoice(false);
    });

    $("#anullo-button").click(function(ev) {
        invoice.reset();
    });

    var specialFlag = false;
    $( "#product-filter" )
    .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {


            event.preventDefault();
        }

        var filter = this.value.trim().toLowerCase();

        if (filter.length > 0) {

            var fullMatch;
            $.each(products[selectedCategoryId], function(index, product) {

                var isMatch = false;


                if (product.productObject.productId.toString().toLowerCase().includes(filter)) {
                    isMatch = true;
                    fullMatch = product.productObject.productId;
                }
                else if (product.productObject.productDescription.toLowerCase().includes(filter)) {
                    isMatch = true;
                    fullMatch = product.productObject.productDescription;
                }

                if (isMatch) {

                    if (!product.div.is(":visible")) {
                        product.div.show("slow");
                    }
                }
                else {

                    if (!product.div.is(":hidden")) {
                        $(`#${product.divId}`).hide("slow");
                    }
                }
            });

            if ($("#product-container").find(".product-item:visible").length == 1) {

                if (specialFlag) {
                    var choosenDiv = $("#product-container").find(".product-item:visible").first();
                    productItemClickHandler(choosenDiv);
                    $("#product-filter").val(fullMatch);
                    event.preventDefault();
                    specialFlag = false;
                }
            }
            else {
                specialFlag = true;
            }
        }
        else {
            $.each(products[selectedCategoryId], function(index, product) {
                if (!product.div.is(":visible")) {
                    product.div.show("slow");
                }
            });
        }
    })
    .autocomplete({
        minLength: 0,
        source: searchProducts,
        focus: function() {
            return false;
        },
        select: function( event, ui ) {

            var filter = this.value.toLowerCase().trim();

            $.each(products[selectedCategoryId], function(index, product) {

                if (!(product.productObject.productId.toString().toLowerCase().includes(filter) ||
                    product.productObject.productDescription.toLowerCase().includes(filter))) {

                        if (!product.div.is(":hidden")) {
                            $(`#${product.divId}`).hide("slow");
                        }
                    }
                    else {
                        if (!product.div.is(":visible")) {
                            product.div.show("slow");
                        }
                    }
            });

            productItemClickHandler($("#product-container").find(".product-item:visible").first());
        }
    });
});

function updateClock () {

    var currentTime = new Date();

    var monthNames = [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];

    var dayNames = ["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"];

    var currentYear = currentTime.getFullYear();
    var currentDate = currentTime.getDate();
    var currentDay = dayNames[currentTime.getDay()-1];
    var currentMonth = monthNames[currentTime.getMonth()];
    var currentHours = currentTime.getHours();
    var currentMinutes = currentTime.getMinutes();

    currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;

    var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

    currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

    currentHours = ( currentHours == 0 ) ? 12 : currentHours;

    var currentTimeString = currentDay + " " + currentDate + " " + currentMonth + " " + currentYear + " " + currentHours + ":" + currentMinutes + " " + timeOfDay;

    $("#clock").text(currentTimeString);
}

function categoryFilterHandler(filterInput) {

    var filter = filterInput.value.trim().toLowerCase();

    if (filter.length > 0) {

        $.each(categories, function(index, cat) {

            if (!(cat.categoryObject.categoryId.toLowerCase().includes(filter) ||
                cat.categoryObject.categoryDescription.toLowerCase().includes(filter))) {

                if (!cat.div.is(":hidden")) {
                    $(`#${cat.divId}`).hide("slow");
                }
            }
            else {
                if (!cat.div.is(":visible")) {
                    cat.div.show("slow");
                }
            }
        });

    }
    else {
        $.each(categories, function(index, cat) {
            if (!cat.div.is(":visible")) {
                cat.div.show("slow");
            }
        });
    }
}

function productFilterHandler(filterInput) {
    var filter = filterInput.value.trim().toLowerCase();

    if (filter.length > 0) {

        var fullMatch;
        $.each(products[selectedCategoryId], function(index, product) {

            var isMatch = false;


            if (product.productObject.productId.toString().toLowerCase().includes(filter)) {
                isMatch = true;
                fullMatch = product.productObject.productId;
            }
            else if (product.productObject.productDescription.toLowerCase().includes(filter)) {
                isMatch = true;
                fullMatch = product.productObject.productDescription;
            }

            if (isMatch) {

                if (!product.div.is(":visible")) {
                    product.div.show("slow");
                }
            }
            else {

                if (!product.div.is(":hidden")) {
                    $(`#${product.divId}`).hide("slow");
                }
            }
        });

        if ($("#product-container").find(".product-item:visible").length == 1) {

            var choosenDiv = $("#product-container").find(".product-item:visible").first();
            productItemClickHandler(choosenDiv);
            $("#product-filter").val(fullMatch);
        }
    }
    else {
        $.each(products[selectedCategoryId], function(index, product) {
            if (!product.div.is(":visible")) {
                product.div.show("slow");
            }
        });
    }
}

function refreshProductsDiv() {

    // Boshatis div-in qe permban artikujt
    $("#product-container").html("");

    // Shton artikujt tek div-i me id product-container
    $.each(products[selectedCategoryId], function(index, productDiv) {
        var product = productDiv.productObject;

        $("#product-container").append(`
            <div class="col-xs-4 col-md-2 product-item" id="product-item-${product.productId}">
                <img class="product-image" src="${product.productImageUrl}">
                <div class="well product-description">
                    ${product.productDescription}
                </div>
            </div>
        `);

        productDiv.div = $(`#product-item-${product.productId}`);
    });

    $(".product-item").click(function() {
        productItemClickHandler($(this));
    });
}


function fetchProducts() {

    // Merr 1 liste me artikuj si JSON per kategorine e klikuar
    $.ajax({
        url: `/getProductsByCategory?categoryId=${selectedCategoryId}`,
        contentType: "application/json",
        success: function(data) {

            products[selectedCategoryId] = [];

            // Boshatis div-in qe permban artikujt
            $("#product-container").html("");

            // Shton artikujt tek div-i me id product-container
            $.each(data, function(index, product) {

                $("#product-container").append(`
                    <div class="col-xs-4 col-md-2 product-item" id="product-item-${product.productId}">
                        <img class="product-image" src="${product.productImageUrl}">
                        <div class="well product-description">
                            ${product.productDescription}
                        </div>
                    </div>
                `);

                products[selectedCategoryId].push({
                    divId: `product-item-${product.productId}`,
                    div: $(`#product-item-${product.productId}`),
                    productObject: product
                });

                searchProducts.push(product.productId);
                searchProducts.push(product.productDescription);
            });

            $(".product-item").click(function() {
                productItemClickHandler($(this));
            });
        }
    });
}


function productItemClickHandler(productItemDiv) {

    var id = productItemDiv.attr("id");

    var clickedProd = $.grep(products[selectedCategoryId],
        function(product) { return product.divId == id; })[0];

    var product = clickedProd.productObject;

    invoice.addProduct(product);
}

function saveInvoice(isItPaid) {

    var storedInvoice = {
        products: [],
        discount: invoice.discount,
        isDiscountPercentage: invoice.isDiscountRate,
        kopertura: invoice.kopetura,
        isKoperturPercentage: invoice.isKoperturaRate,
        ePaguar: isItPaid
    };

    $.each(invoice.invoiceProducts, function(index, product) {

        if (product.value) {
            storedInvoice.products.push({
                productId: product.productObject.productId,
                quantity: 1,
                price: product.price,
                value: product.value
            });
        }
        else {
            storedInvoice.products.push({
                productId: product.productObject.productId,
                quantity: product.quantity
            });
        }
    });

    var text = JSON.stringify(storedInvoice);
    var blob = new Blob([text], {type: "text/plain;charset=utf-8"});

    var now = new Date();
    timestamp = now.getDay() + "-" + now.getMonth() + "-" + now.getYear() +
        "_" + now.getHours() + "-" + now.getMinutes() + "-" + now.getSeconds();

    saveAs(blob, `fature_${timestamp}.txt`);
}

function round(decimalValue) {
    return Math.round(decimalValue * 100) / 100;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
