$.mockjax({
  url: "/getUsername",
  responseText: {
    username: "Admin Admin"
  }
});

$.mockjax({
  url: "/getCategories",
  responseText: [
    {
      categoryId: "PO1",
      categoryImageUrl: "img/pantallona_category.png",
      categoryDescription: "Pantallona per femra"
    },
    {
      categoryId: "PO2",
      categoryImageUrl: "img/pantallona_category.png",
      categoryDescription: "Pantallona per meshkuj"
    }
  ]
});

$.mockjax({
    url: "/getProductsByCategory?categoryId=PO1",
    responseText: [
        {
            productId: 1,
            productDescription: "Distressed Skinny Jeans",
            productImageUrl: "img/Distressed_Skinny_Jeans.jpg",
            productPrice: 29.90
        },
        {
            productId: 2,
            productDescription: "High-Rise Skinny Jeans",
            productImageUrl: "img/high_rise_skinny_jeans.jpg",
            productPrice: 15.00
        }
    ]
});

$.mockjax({
    url: "/getProductsByCategory?categoryId=PO2",
    responseText: [
        {
            productId: 3,
            productDescription: "Black Keys Destroyed Jeans",
            productImageUrl: "img/menjeans1.jpg",
            productPrice: 54.00
        },
        {
            productId: 4,
            productDescription: "Camo Print Moto Style Jeans",
            productImageUrl: "img/menjeans2.jpg",
            productPrice: 29.90
        }
    ]
});
